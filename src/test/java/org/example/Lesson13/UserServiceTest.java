package org.example.Lesson13;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserService userService;

    @Test
    public void testIsUserActive() {
        String username = "testUser";
        User activeUser = new User();
        activeUser.setUsername(username);
        activeUser.setActive(true);

        when(userRepository.findByUsername(username)).thenReturn(activeUser);

        assertTrue(userService.isUserActive(username));
    }

    @Test
    public void testDeleteUser_UserExists() {
        Long userId = 1L;
        User existingUser = new User();
        existingUser.setId(userId);

        when(userRepository.findUserId(userId)).thenReturn(existingUser);

        assertDoesNotThrow(() -> userService.deleteUser(userId));
    }

    @Test
    public void testDeleteUser_UserNotFound() {
        Long userId = 1L;

        when(userRepository.findUserId(userId)).thenReturn(null);

        assertThrows(Exception.class, () -> userService.deleteUser(userId));
    }

    @Test
    public void testGetUser_UserExists() throws Exception {
        Long userId = 1L;
        User existingUser = new User();
        existingUser.setId(userId);

        when(userRepository.findUserId(userId)).thenReturn(existingUser);

        assertEquals(existingUser, userService.getUser(userId));
    }

    @Test
    public void testGetUser_UserNotFound() {
        Long userId = 1L;

        when(userRepository.findUserId(userId)).thenReturn(null);

        assertThrows(Exception.class, () -> userService.getUser(userId));
    }
}