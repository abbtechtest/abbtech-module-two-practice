package org.example.Lesson12;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    @Test
    public void testAddition() {
        Calculator calculator = new Calculator();
        assertEquals(5, calculator.add(2, 3));
        assertEquals(-1, calculator.add(-4, 3));
        assertEquals(0, calculator.add(0, 0));
    }

    @Test
    public void testSubtraction() {
        Calculator calculator = new Calculator();
        assertEquals(1, calculator.subtract(4, 3));
        assertEquals(-7, calculator.subtract(-4, 3));
        assertEquals(0, calculator.subtract(0, 0));
    }

    @Test
    public void testMultiplication() {
        Calculator calculator = new Calculator();
        assertEquals(6, calculator.multiply(2, 3));
        assertEquals(-12, calculator.multiply(-4, 3));
        assertEquals(0, calculator.multiply(0, 0));
    }

    @Test
    public void testDivision() {
        Calculator calculator = new Calculator();
        assertEquals(3, calculator.divide(9, 3));
        assertEquals(-2, calculator.divide(-10, 5));
        assertEquals(0, calculator.divide(0, 5));
        assertThrows(ArithmeticException.class, () -> calculator.divide(10, 0));
    }

}