package org.example.Lesson13;

public interface UserRepository {

    User findByUsername(String username);

    User findUserId(Long userId);
}
